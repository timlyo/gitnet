FROM base/archlinux

RUN pacman -Syy
RUN pacman -S --noconfirm git

WORKDIR gitnet

ADD test .
ADD startup.sh .
ADD . .

# Make test commit
RUN cd test_repo &&\
    date >> time &&\
    git config --global user.email "test@example.com" &&\
    git config --global user.name "test" &&\
    git add time &&\
    git commit -m "Updated time"

ADD target/debug/gitnet .

ENTRYPOINT ["bash", "startup.sh"]
