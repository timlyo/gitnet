#!/usr/bin/env bash

docker build . -t gitnet-test

docker kill test1
docker kill test2
docker kill test3
#docker kill test4

docker rm test1
docker rm test2
docker rm test3
#docker rm test4

docker run --name test1 -d --network=gitnet gitnet-test "Frank" --daemon
docker run --name test2 -d --network=gitnet gitnet-test "Bob" --daemon
docker run --name test3 -d --network=gitnet gitnet-test "Steve Wadey" --daemon
#docker run --name test4 -d --network=gitnet gitnet-test --daemon
