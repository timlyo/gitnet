use crate::host::Host;
use std::net::IpAddr;

pub struct Data {
    pub host: Host,
    pub peers: Vec<Host>,
}

impl Data {
    pub fn new(machine_address: IpAddr) -> Data {
        Data {
            host: Host::this_machine(machine_address),
            peers: vec![],
        }
    }
}
