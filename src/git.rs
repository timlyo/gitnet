use crate::data::Data;
use crate::project::Project;
use std::env;
use std::io;
use std::path::PathBuf;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;
use std::time::Duration;
use walkdir::WalkDir;

pub fn get_current_repo() -> Option<PathBuf> {
    let mut current_directory = match env::current_dir() {
        Ok(d) => d,
        Err(e) => return None,
    };

    if current_directory.join(".git").exists() {
        return Some(current_directory);
    };


    while current_directory.pop() {
        if current_directory.join(".git").exists() {
            return Some(current_directory);
        };
    }

    None
}

pub fn watch_projects(data: Arc<Mutex<Data>>) -> Result<(), io::Error> {
    println!("Watching projects");
    loop {
        add_projects_to_data(&data)?;
        thread::sleep(Duration::from_secs(30));
    }
}

pub fn add_projects_to_data(data: &Arc<Mutex<Data>>) -> Result<(), io::Error> {
    let mut projects = find_all_git_repos()?;

    for project in &mut projects {
        if let Ok(branches) = Project::get_repo_status(project) {
            project.branches = branches;
        }
    }

    let mut data = data.lock().expect("Couldn't unlock Mutex");
    data.host.projects = projects;

    Ok(())
}

pub fn find_all_git_repos() -> Result<Vec<Project>, io::Error> {
    let git_dirs = WalkDir::new("/").into_iter().filter(is_git);

    let git_dirs = git_dirs
        .filter_map(|dir| dir.ok())
        .filter_map(|dir| Project::from_dir_entry(dir));

    Ok(git_dirs.collect())
}

fn is_git(dir: &Result<walkdir::DirEntry, walkdir::Error>) -> bool {
    match dir {
        Ok(dir) => dir
            .file_name()
            .to_str()
            .map(|d| d == ".git")
            .unwrap_or(false),
        Err(_) => false,
    }
}

pub fn get_global_name() -> Result<String, git2::Error> {
    let home = dirs::home_dir()
        .expect("Couldn't get home")
        .join(".gitconfig");
    let config = git2::Config::open(&home)?;

    config.get_string("user.name")
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;

    #[test]
    fn test_is_run_from_repo_finds_repo() {
        let result = get_current_repo();

        assert!(result.is_some());
    }

    #[test]
    fn test_is_run_from_repo_returns_false_when_not_in_repo() {
        let new_path = Path::new("/home");
        let new_path = dirs::home_dir().unwrap();
        env::set_current_dir(new_path);

        let result = get_current_repo();

        assert!(result.is_none());
    }

    #[test]
    fn test_find_all_git_repos() {
        let repos = find_all_git_repos().unwrap();

        dbg!(repos);

        //        assert!(repos.len() != 0);
    }

    #[test]
    fn test_get_global_name() {
        let name = get_global_name();

        println!("{:?}", name);

        assert!(name.is_ok())
    }
}
