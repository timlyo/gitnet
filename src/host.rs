use crate::git;
use crate::project::Project;
use crate::system::normalise_to_hostname;
use std::net::IpAddr;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Host {
    pub name: String,
    pub is_this_machine: bool,
    pub address: Option<IpAddr>,
    pub projects: Vec<Project>,
}

impl Host {
    pub fn this_machine(address: IpAddr) -> Host {
        let name = git::get_global_name().unwrap_or("Couldn't get name".into());
        dbg!(&name);
        Host {
            name: normalise_to_hostname(name),
            is_this_machine: true,
            address: Some(address),
            projects: vec![],
        }
    }
}
