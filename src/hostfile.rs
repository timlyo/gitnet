use crate::host::Host;
use std::collections::HashSet;
use std::fs;
use std::fs::File;
use std::io;
use std::io::{Read, Write};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HostLine {
    pub address: String,
    pub names: HashSet<String>,
}

fn parse_line(line: &str) -> Option<HostLine> {
    if line.starts_with("#") || line == "" {
        return None;
    }

    let parts: Vec<String> = line
        .split(" ")
        .filter(|line| !line.is_empty()) // Handle multiple spaces between terms
        .map(str::to_string)
        .collect();

    let names: HashSet<String> = parts[1..].to_vec().into_iter().collect();

    Some(HostLine {
        address: parts[0].to_string(),
        names,
    })
}

pub fn get_host_lists() -> Result<Vec<HostLine>, io::Error> {
    let mut file = File::open("/etc/hosts")?;
    let mut content = String::new();
    let read = file.read_to_string(&mut content)?;

    let entries = content.lines().filter_map(parse_line);

    Ok(entries.collect())
}

pub fn add_host(host: &Host) -> Result<Option<()>, io::Error> {
    if host.address.is_none() {
        return Ok(None);
    }

    let hosts = get_host_lists()?;

    let already_in_file = hosts
        .iter()
        .filter(|h| h.names.contains(&host.name))
        .count()
        > 0;

    if already_in_file {
        return Ok(None);
    }

    let mut file = fs::OpenOptions::new().append(true).open("/etc/hosts")?;

    let line = format!("\n{} {}", host.address.unwrap(), host.name);

    file.write_all(line.as_bytes());

    Ok(Some(()))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::system;

    #[test]
    fn test_get_host_lists() {
        let hosts = get_host_lists();

        dbg!(hosts);
    }

    #[test]
    fn test_add_host() {
        let host = Host::this_machine(system::get_ip().unwrap());
        let result = add_host(&host);

        result.unwrap();

        let host_from_file = get_host_lists()
            .unwrap()
            .into_iter()
            .filter(|hl| hl.names.contains(&host.name))
            .collect::<Vec<HostLine>>();

        assert_eq!(host_from_file.len(), 1);
    }
}
