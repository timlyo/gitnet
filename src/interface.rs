use crate::git;
use crate::http;
use clap::ArgMatches;
use clap::{App, Arg};
use std::env;
use crate::project::Project;
use crate::host::Host;

pub fn create_cmdline<'a>() -> App<'a, 'a> {
    App::new("Gitnet")
        .about("Connect git hosts in a network")
        .arg(
            Arg::with_name("daemon")
                .short("d")
                .long("daemon")
                .help("Run gitnet daemon"),
        )
}

fn print_project(local: &Project, peers: &Vec<Host>) {
    for peer in peers{
        println!("{}:{}", peer.name, peer.address.unwrap().to_string());
        if let Some(matching_project) = peer.projects.iter()
            .filter(|p| p.name == local.name).next(){

            for local_branch in &local.branches{
                for remote_branch in &matching_project.branches{
                    if remote_branch.name == local_branch.name{
                        println!("  {}", local_branch.name);
                        let commit_diff = local_branch.commit_count as u32 - remote_branch.commit_count as u32;
                        println!("    diff={}", commit_diff);
                    }
                }
            }
        }else{
            println!("No projects in common");
        }
    }
}

pub fn display(args: &ArgMatches) -> reqwest::Result<()> {
    let host = http::get_host()?;
    let peers = http::get_peers()?;

    println!("Host machine: {}", host.name);
    let mut current_project = "Unknown".to_string();
    let current_repo = git::get_current_repo();

    if let Some(current_repo) = current_repo {
        let current_git_dir = current_repo.join(".git");
        let project = host.projects.iter()
            .filter(|p| p.location == current_git_dir)
            .next();

        if let Some(project) = project {
            print_project(project, &peers);
        }
    } else {
        println!("Host machine: {}", host.name);
        print!("Host address: {}", host.address.unwrap());
        println!("List of current projects");
        for project in host.projects {
            println!("Project name: {}", project.name);
            println!(
                "Project location: {}",
                project.location.to_str().unwrap_or("Unknown")
            );
        }
        println!("Peers connected: {}", peers.len());
        for peer in peers {
            println!("Name: {}", peer.name);
            println!("Address: {}", peer.address.unwrap());
            println!("List of current projects");
            for project in peer.projects {
                println!("Project name: {}", project.name);
                println!("Project location: {}", project.name);
            }
        }

        //dbg!(host);
    }

    Ok(())
}
