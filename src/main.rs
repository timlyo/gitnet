#![feature(proc_macro_hygiene, decl_macro)]

extern crate rocket;
#[macro_use]
extern crate serde_derive;
extern crate clap;
extern crate dirs;
extern crate ipnet;
extern crate iprange;
extern crate pnet;
extern crate rayon;
extern crate walkdir;

use crate::data::Data;
use clap::ArgMatches;
use std::sync::Arc;
use std::sync::Mutex;
use std::thread;

mod data;
mod git;
mod host;
mod hostfile;
mod http;
mod interface;
mod project;
mod system;

fn main() {
    let ip = system::get_ip().expect("Couldn't get docker0 ip");
    let args = interface::create_cmdline().get_matches();
    let data = Arc::new(Mutex::new(Data::new(ip)));

    if args.is_present("daemon") {
        spawn_daemon(data.clone());
    } else {
        display_interface(&args);
    }
}

fn spawn_daemon(data: Arc<Mutex<Data>>) {
    let http_data = data.clone();
    let watch_projects_data = data.clone();
    let ip_scan_data = data.clone();

    let ip_range = "172.16.17.0/24".to_string();

    let threads = vec![
        thread::spawn(move || {
            http::start(http_data);
        }),
        thread::spawn(move || {
            git::watch_projects(watch_projects_data);
        }),
        thread::spawn(move || {
            http::watch_ips(ip_range, ip_scan_data);
        }),
    ];

    for thread in threads {
        thread.join();
    }
}

fn display_interface(args: &ArgMatches) {
    if let Err(e) = interface::display(args) {
        dbg!(e);
    }
}
